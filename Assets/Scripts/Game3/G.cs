using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

namespace RPG
{

public class G : MonoBehaviour
{
  public static G self = null;

  public Unit player;

  public int enemy_count;

  public Text hero_hp;
  public Text enemy_hp;

  void Awake()
  {
    if(self == null)
      self = this;
  }

  void Update()
  {
    if(Input.GetMouseButtonDown(0))
    {
      RaycastHit hit;
      Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
      if(Physics.Raycast(ray, out hit))
      {
        if(hit.transform.gameObject.tag == "Enemy")
          player.SetTarget(hit.transform.gameObject.GetComponent<Unit>());
        else if(hit.transform.gameObject.tag == "Ground")
          player.SetTarget(hit.point);
      }
    }

    hero_hp.text = string.Format("HP: {0}/{1}\nLevel: {2} ({3}/{4})", (int)player.hp, (int)player.hp_max, player.level, (int)player.xp, (int)player.GetXPForLevelUP());

    if(player.target != null)
      enemy_hp.text = string.Format("HP: {0}/{1}", (int)player.target.hp, (int)player.target.hp_max);

    enemy_hp.gameObject.SetActive(player.target != null);

    if(enemy_count == 0 || player == null)
      SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
  }
}

}
