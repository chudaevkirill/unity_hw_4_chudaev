﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace RPG
{
    public class Upgrade : MonoBehaviour
    {
        public Unit unit;

        public void HealUp()
        {
            transform.gameObject.SetActive(false);
            unit.hp_max += Mathf.Floor(unit.hp_max * 0.2f);
            unit.hp = unit.hp_max;


        }
        public void PowerUp()
        {
            transform.gameObject.SetActive(false);
            unit.attack += unit.attack_per_level;
        }
        public void RegenUp()
        {
            transform.gameObject.SetActive(false);
            unit.regen_hp_speed += 5f;
        }
    }
}
