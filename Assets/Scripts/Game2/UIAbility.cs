using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game2
{

public class UIAbility : MonoBehaviour
{
  public Text name_text;
  public Text description_text;
  Unit.Ability ability;
  Action<Unit.Ability> on_select;
  Button button;
  public Unit unit;

  void Awake()
  {
    button = GetComponent<Button>();
  }

  public void Init(Unit.Ability ability, Action<Unit.Ability> on_select)
  {
    this.ability = ability;
    this.on_select = on_select;
    name_text.text = ability.name;
    if(ability.mp > 0)
      name_text.text += " " + ability.mp;
    button.onClick.AddListener(OnClick);
  }

  void OnClick()
  {
    on_select?.Invoke(ability);
  }

  void Update()
  {
    if(ability == null)
      return;

    button.interactable = ability.mp <= unit.mp;

    if(button.interactable)
    {
      if(ability.target_type == Unit.Ability.TargetType.ENEMY)
        description_text.text = $"Урон: {ability.damage}";
      else
        description_text.text = $"Здоровье: +{-ability.damage}";
    }
    else
    {
      description_text.text = $"Нужно: {ability.mp} маны";
    }
  }
}

}
